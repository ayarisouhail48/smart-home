class AppRouter {
  static const String splash = "/";
  static const String login = "/login";
  static const String register = "/register";
  static const String homePage = "/homepage";
  static const String house = "/house";
  static const String registerHouse = "/houseRegister";
}
