import 'api_error.dart';
import 'api_result.dart';

class ApiResponse<T> {
  int? status;
  ApiResult<T>? result;
  ApiError? error;

  bool get hasData => result!.data != null;
  bool get hasError => error != null;
  bool get isSuccessed => status == 1;
  bool get isFailed => status == 0;

  ApiResponse();

  factory ApiResponse.withResultAsList({
    required List<dynamic> response,
    required ApiResult<T> Function(dynamic json) resultConverter,
  }) {
    return ApiResponse()
      ..status = 1
      ..result = resultConverter(response);
  }

  factory ApiResponse.withResult({
    required Map<String, dynamic> response,
    required ApiResult<T> Function(dynamic json) resultConverter,
  }) {
    var status = response['status'];

    if (status == null || status as int == 1) {
      /// Currently in API, Status = null means success ( auth api)
      return ApiResponse()
        ..status = 1
        ..result = resultConverter(response);
    } else {
      return ApiResponse()
        ..status = status
        ..error = ApiError.fromJson(response);
    }
  }

  /// Empty response implies that API Result was success
  factory ApiResponse.success() {
    return ApiResponse()..status = 1;
  }

  factory ApiResponse.withError(dynamic error) {
    return ApiResponse()
      ..status = 0
      ..error = ApiError(error: error);
  }
}
