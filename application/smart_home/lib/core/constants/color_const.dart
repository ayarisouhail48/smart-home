import 'package:flutter/material.dart';

class ColorConst {
  static const Color blueFaded = Color.fromARGB(255, 186, 237, 244);
  static const Color tealEnhanced = Color.fromARGB(255, 89, 158, 173);
  static const LinearGradient softFeeling = LinearGradient(
    begin: Alignment.centerLeft,
    end: Alignment.centerRight,
    colors: [
      tealEnhanced,
      blueFaded,
    ],
  );
}
