import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class DropdownButtonSH extends StatelessWidget {
  final String label;
  final Function(int?) onChanged;
  final String? Function(int?) validator;
  final List<DropdownMenuItem<int>> listDropdownItems;

  const DropdownButtonSH({
    Key? key,
    required this.label,
    required this.onChanged,
    required this.validator,
    required this.listDropdownItems,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField<int>(
      isExpanded: true,
      decoration: InputDecoration(
        labelText: label,
        floatingLabelBehavior: FloatingLabelBehavior.auto,
        fillColor: Colors.grey.withOpacity(0.2),
        isDense: true,
        filled: true,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(12.r),
          borderSide: BorderSide.none,
        ),
        contentPadding: EdgeInsets.symmetric(
          horizontal: 10.w,
          vertical: 10.h,
        ),
      ),
      items: listDropdownItems,
      onChanged: onChanged,
      validator: validator,
    );
  }
}

DropdownMenuItem<int> buildDropdownItem({
  required int value,
  required String title,
}) {
  return DropdownMenuItem(
    value: value,
    child: Text(title),
  );
}
