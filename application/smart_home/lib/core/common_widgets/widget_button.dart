import 'package:flutter/material.dart';

class WidgetButtonSH extends StatelessWidget {
  const WidgetButtonSH({
    Key? key,
    required this.onTap,
    required this.child,
    this.height,
    this.backgroundColor,
  }) : super(key: key);

  final Function() onTap;
  final Widget? child;
  final double? height;
  final Color? backgroundColor;

  @override
  Widget build(BuildContext context) {
    final buttonSize = height ?? 20;
    return Container(
      height: buttonSize,
      decoration: BoxDecoration(
        color: backgroundColor ?? const Color(0xFFF1F2F6),
        borderRadius: BorderRadius.circular(buttonSize / 5),
        boxShadow: [
          BoxShadow(
            color: backgroundColor != null
                ? backgroundColor!.withOpacity(0.5)
                : const Color(0xFFDADFF0),
            offset: Offset(buttonSize / 10, buttonSize / 10),
            blurRadius: buttonSize / 10,
          ),
        ],
      ),
      child: MaterialButton(
        splashColor: Colors.transparent,
        onPressed: onTap,
        child: child,
      ),
    );
  }
}
