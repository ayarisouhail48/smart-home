import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TextFormFieldSH extends StatelessWidget {
  const TextFormFieldSH({
    Key? key,
    required this.labelText,
    this.labelBehavior,
    required this.hintText,
    this.defaultText,
    this.prefixIcon,
    required this.onChanged,
    required this.validator,
  }) : super(key: key);

  final String labelText;
  final FloatingLabelBehavior? labelBehavior;
  final String hintText;
  final String? defaultText;
  final Widget? prefixIcon;
  final Function(String?) onChanged;
  final String? Function(String?) validator;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      initialValue: defaultText,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      onChanged: onChanged,
      validator: validator,
      style: TextStyle(fontSize: 16.sp),
      decoration: InputDecoration(
        floatingLabelBehavior: labelBehavior ?? FloatingLabelBehavior.auto,
        labelText: labelText,
        hintText: hintText,
        fillColor: Colors.grey.withOpacity(0.2),
        prefixIcon: prefixIcon ?? Container(),
        isDense: true,
        filled: true,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(12.r),
          borderSide: BorderSide.none,
        ),
        contentPadding: EdgeInsets.symmetric(
          horizontal: 10.w,
          vertical: 10.h,
        ),
      ),
    );
  }
}
