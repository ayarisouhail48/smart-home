import 'package:flutter/material.dart';
import 'package:smart_home/core/constants/color_const.dart';

class CircularIconButtonSH extends StatelessWidget {
  const CircularIconButtonSH({
    Key? key,
    required this.onTap,
    required this.icon,
    this.size,
  }) : super(key: key);

  final Function() onTap;
  final IconData icon;
  final double? size;

  @override
  Widget build(BuildContext context) {
    final buttonSize = size ?? 20;
    return Container(
      width: buttonSize * 2,
      height: buttonSize * 2,
      decoration: BoxDecoration(
        color: const Color(0xFFF1F2F6),
        borderRadius: BorderRadius.circular(buttonSize),
        boxShadow: [
          BoxShadow(
            color: const Color(0xFFDADFF0),
            offset: Offset(buttonSize / 10, buttonSize / 10),
            blurRadius: buttonSize / 5,
          ),
        ],
      ),
      child: IconButton(
        onPressed: onTap,
        icon: Icon(
          icon,
          color: ColorConst.tealEnhanced,
          size: buttonSize * 1.2,
        ),
      ),
    );
  }
}
