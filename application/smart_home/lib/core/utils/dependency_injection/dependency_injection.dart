import 'package:get_it/get_it.dart';
import 'package:smart_home/core/api/dio/dio_http_client.dart';
import 'package:smart_home/core/api/iot/amqp_client.dart';
import 'package:smart_home/features/authentication/data/rest_api/authentication_repository_impl.dart';
import 'package:smart_home/features/authentication/data/rest_api/user_repository_impl.dart';
import 'package:smart_home/features/smart_home/data/rest_api/house_data_repository_impl.dart';
import 'package:smart_home/features/smart_home/data/rest_api/house_repository_impl.dart';

final GetIt injector = GetIt.instance;

Future<void> inject() async {
  /// Register HttpClient & AmqpClient as singletons
  injector.registerSingleton<DioHttpClient>(DioHttpClient());
  injector.registerSingleton<AmqpClient>(AmqpClient());

  /// Register repositories as singletons
  injector.registerSingleton<RestApiAuthenticationRepository>(
      RestApiAuthenticationRepository());
  injector.registerSingleton<RestApiUserRepository>(RestApiUserRepository());
  injector.registerLazySingleton(() => RestApiHouseRepository());
  injector.registerLazySingleton(() => RestApiHouseDataRepository());
}
