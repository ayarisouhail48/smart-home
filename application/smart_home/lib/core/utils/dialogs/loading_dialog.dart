import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:smart_home/core/constants/color_const.dart';

class LoadingDialog extends StatelessWidget {
  const LoadingDialog({Key? key, required this.message}) : super(key: key);
  final String message;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false, // Prevent navigator popping when loading
      child: Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.r),
        ),
        child: Padding(
          padding: EdgeInsets.all(8.r),
          child: Wrap(
            alignment: WrapAlignment.center,
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(4.r),
                child: Image.asset('lib/asset/loading.gif'),
              ),
              SizedBox(height: 18.h),
              Text(
                message,
                style: TextStyle(
                  color: ColorConst.tealEnhanced,
                  fontSize: 18.sp,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
