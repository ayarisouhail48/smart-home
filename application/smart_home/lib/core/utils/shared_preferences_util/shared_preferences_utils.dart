import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesUtils {
  static Future<SharedPreferences> get _instance async =>
      _prefsInstance = await SharedPreferences.getInstance();
  static late SharedPreferences _prefsInstance;

  static Future<SharedPreferences> init() async {
    _prefsInstance = await _instance;
    return _prefsInstance;
  }

  static String? getString({required String key}) {
    return _prefsInstance.getString(key);
  }

  static Future<bool> setString({
    required String key,
    required String value,
  }) async {
    var prefs = await _instance;
    return prefs.setString(key, value);
  }

  static Future<bool> removeString({required String key}) async {
    var prefs = await _instance;
    return prefs.remove(key);
  }
}
