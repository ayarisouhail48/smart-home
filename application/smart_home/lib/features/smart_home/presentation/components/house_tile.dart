import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:shimmer/shimmer.dart';
import 'package:smart_home/core/constants/color_const.dart';
import 'package:smart_home/features/smart_home/domain/entities/house.dart';
import 'package:smart_home/features/smart_home/presentation/views/house/house_screen.dart';

import '../utils/style_constant.dart';

class HouseTile extends StatelessWidget {
  final House house;
  const HouseTile({
    Key? key,
    required this.house,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double widgetHeight = 115.h;
    double widgetWidth = 336.w;
    return GestureDetector(
      onTap: () => Navigator.push(
        context,
        PageRouteBuilder(
          transitionDuration: const Duration(milliseconds: 500),
          pageBuilder: (BuildContext context, Animation<double> animation,
              Animation<double> secondaryAnimation) {
            return HouseScreen(house: house);
          },
          transitionsBuilder: (BuildContext context,
              Animation<double> animation,
              Animation<double> secondaryAnimation,
              Widget child) {
            return Align(
              child: FadeTransition(
                opacity: animation,
                child: child,
              ),
            );
          },
        ),
      ),
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: <Widget>[
          ShaderMask(
            shaderCallback: (bounds) {
              return ColorConst.softFeeling.createShader(bounds);
            },
            child: Container(
              height: widgetHeight,
              margin: kHouseTileMargin,
              decoration: BoxDecoration(
                gradient: ColorConst.softFeeling,
                image: const DecorationImage(
                  image: AssetImage('lib/asset/home_interior.jpg'),
                  fit: BoxFit.cover,
                ),
                borderRadius: BorderRadius.circular(12.0),
                boxShadow: [
                  BoxShadow(
                    offset: const Offset(3, 3),
                    blurRadius: 1.5,
                    color: ColorConst.tealEnhanced.withOpacity(0.4),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: widgetHeight / 8,
            left: widgetWidth / 12,
            child: Hero(
              tag: 'house_name_${house.id}',
              child: Material(
                type: MaterialType.transparency,
                child: Text(
                  house.name,
                  style: kHouseTileTitleStyle,
                ),
              ),
            ),
          ),
          Positioned(
            bottom: widgetHeight / 8 * 3,
            left: widgetWidth / 12,
            child: Row(
              children: [
                const Icon(
                  Icons.qr_code_outlined,
                  color: Colors.white,
                ),
                const SizedBox(width: 6.0),
                Text(
                  'Code: ' + house.code,
                  style: kHouseTileSubTitleStyle,
                ),
              ],
            ),
          ),
          Positioned(
            bottom: widgetHeight / 8,
            left: widgetWidth / 12,
            child: SizedBox(
              width: widgetWidth / 2,
              child: Row(
                children: [
                  const Icon(
                    Icons.memory_outlined,
                    color: Colors.white,
                  ),
                  const SizedBox(width: 6.0),
                  Text(
                    'Device Count: ${house.sensors.length}',
                    style: kHouseTileSubTitleStyle,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class ShimmerHouseTile extends StatelessWidget {
  const ShimmerHouseTile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: Colors.grey.shade300,
      highlightColor: Colors.grey.shade100,
      child: Container(
        height: 115.h,
        margin: kHouseTileMargin,
        decoration: BoxDecoration(
          color: Colors.black,
          borderRadius: BorderRadius.circular(12.0),
        ),
      ),
    );
  }
}
