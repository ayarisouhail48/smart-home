import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:smart_home/core/common_widgets/widget_button.dart';
import 'package:smart_home/core/constants/color_const.dart';
import 'package:smart_home/features/authentication/bloc/authentication_bloc.dart';
import 'package:smart_home/features/smart_home/bloc/house/house_manage_bloc.dart';
import 'package:smart_home/features/smart_home/presentation/components/house_tile.dart';
import 'package:smart_home/features/smart_home/presentation/utils/style_constant.dart';

import '../../../../../router.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final kAppBarTitleStyle = TextStyle(
    color: ColorConst.tealEnhanced,
    fontSize: 16.sp,
    fontWeight: FontWeight.w500,
  );
  late final Image backgroundImg;
  late final RefreshController _refreshController;

  @override
  void initState() {
    _refreshController = RefreshController();
    BlocProvider.of<HouseManageBloc>(context).add(FetchHouses());
    backgroundImg = Image.asset("lib/asset/background_home.jpg");
    super.initState();
  }

  @override
  void didChangeDependencies() {
    precacheImage(backgroundImg.image, context);
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SmartRefresher(
          controller: _refreshController,
          onRefresh: () {
            BlocProvider.of<HouseManageBloc>(context).add(FetchHouses());
            _refreshController.refreshCompleted();
          },
          child: CustomScrollView(
            slivers: <Widget>[
              SliverAppBar(
                backgroundColor: Colors.white,
                pinned: true,
                title: BlocBuilder<AuthenticationBloc, AuthenticationState>(
                  builder: (context, state) {
                    return Text(
                      'Welcome Home, ${state.user?.username} !',
                      style: kAppBarTitleStyle,
                    );
                  },
                ),
                actions: [
                  _buildSideNavigationPopup(),
                ],
                floating: false,
                expandedHeight: 0.2.sh,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.vertical(
                    bottom: Radius.circular(8.r),
                  ),
                ),
                flexibleSpace: FlexibleSpaceBar(
                  background: ClipRRect(
                    borderRadius: BorderRadius.vertical(
                      bottom: Radius.circular(20.r),
                    ),
                    child: Opacity(
                      opacity: 0.25,
                      child: Container(
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: backgroundImg.image,
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              SliverList(
                delegate: SliverChildListDelegate(
                  <Widget>[
                    SizedBox(height: 8.h),
                    Padding(
                      padding: EdgeInsets.all(12.r),
                      child: Text(
                        'Houses',
                        style: TextStyle(
                          color: ColorConst.tealEnhanced,
                          fontWeight: FontWeight.w700,
                          fontSize: 22.sp,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              BlocBuilder<HouseManageBloc, HouseManageState>(
                builder: (context, snapshot) {
                  if (snapshot.status == HouseManageStatus.fetching) {
                    return SliverList(
                      delegate: SliverChildListDelegate(
                        List.generate(3, (_) {
                          return const ShimmerHouseTile();
                        }),
                      ),
                    );
                  } else if (snapshot.status == HouseManageStatus.failed) {
                    return _buildWarning(hasError: true);
                  } else if (snapshot.status ==
                      HouseManageStatus.fetchSuccess) {
                    if (snapshot.houses!.isNotEmpty) {
                      List<Widget> widgets = List.generate(
                        snapshot.houses!.length,
                        (index) {
                          return HouseTile(house: snapshot.houses![index]);
                        },
                      );
                      widgets.add(_buildAddHouseButton());
                      return SliverList(
                        delegate: SliverChildListDelegate(widgets),
                      );
                    } else {
                      return _buildWarning(hasError: false);
                    }
                  }
                  // Default:
                  return _buildWarning(hasError: true);
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildWarning({required bool hasError}) {
    return SliverToBoxAdapter(
      child: Center(
        child: Column(
          children: [
            SizedBox(height: 24.h),
            SizedBox(
              width: 0.66.sw,
              child: Image.asset('lib/asset/construct.png'),
            ),
            SizedBox(height: 12.h),
            Text(
              hasError
                  ? 'Unknown error, pull down to retry!'
                  : 'You have no house at the moment !',
              // 'You have no house at the moment !',
              style: TextStyle(
                color: ColorConst.tealEnhanced,
                fontSize: 18.sp,
                fontWeight: FontWeight.w300,
              ),
            ),
            hasError ? Container() : _buildAddHouseButton(),
          ],
        ),
      ),
    );
  }

  Widget _buildAddHouseButton() {
    return Padding(
      padding: EdgeInsets.only(top: 24.r, bottom: 12.r),
      child: Align(
        child: WidgetButtonSH(
          height: 40.h,
          onTap: () => Navigator.pushNamed(
            context,
            AppRouter.registerHouse,
          ),
          child: Text(
            'Register new house',
            style: kHouseDetailsStyle,
          ),
        ),
      ),
    );
  }

  Widget _buildSideNavigationPopup() {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: PopupMenuButton(
        child: const Icon(
          Icons.menu_rounded,
          color: ColorConst.tealEnhanced,
        ),
        elevation: 8.sp,
        offset: Offset(-8.r, 16.r),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12.0),
        ),
        itemBuilder: (context) => [
          PopupMenuItem(
            onTap: () {},
            child: _buildPopupMenuItem(Icons.people_rounded, 'Account'),
          ),
          PopupMenuItem(
            /// Navigate to Settings Page
            onTap: () {},
            child: _buildPopupMenuItem(Icons.settings, 'Settings'),
          ),
          PopupMenuItem(
            onTap: () {},
            child: _buildPopupMenuItem(Icons.help_center_rounded, 'Help'),
          ),
          PopupMenuItem(
            onTap: () async {
              await Future.delayed(const Duration(milliseconds: 300));
              Navigator.pushNamedAndRemoveUntil<void>(
                context,
                AppRouter.login,
                (route) => false,
              );
              context.read<AuthenticationBloc>().add(AuthenticationLogout());
            },
            child: _buildPopupMenuItem(Icons.logout_rounded, 'Log out'),
          ),
        ],
      ),
    );
  }

  Widget _buildPopupMenuItem(IconData icon, String title) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(title, style: const TextStyle(color: ColorConst.tealEnhanced)),
        Icon(icon, color: ColorConst.tealEnhanced),
      ],
    );
  }
}
