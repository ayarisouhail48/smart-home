// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'device.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Device _$DeviceFromJson(Map<String, dynamic> json) => Device(
      idx: json['idx'] as int,
      id: json['id'] as int,
      name: json['name'] as String,
      type: json['type'] as String,
    );

Map<String, dynamic> _$DeviceToJson(Device instance) => <String, dynamic>{
      'idx': instance.idx,
      'id': instance.id,
      'name': instance.name,
      'type': instance.type,
    };

DeviceData _$DeviceDataFromJson(Map<String, dynamic> json) => DeviceData(
      houseSensorId: json['houseSensorId'] as int,
      sensorType: json['sensorType'] as String,
      status: json['status'] as bool?,
      temp: (json['temp'] as num?)?.toInt(),
      hum: (json['hum'] as num?)?.toDouble(),
      gasConcentration: (json['gasConcentration'] as num?)?.toDouble(),
    );

Map<String, dynamic> _$DeviceDataToJson(DeviceData instance) =>
    <String, dynamic>{
      'houseSensorId': instance.houseSensorId,
      'sensorType': instance.sensorType,
      'status': instance.status,
      'temp': instance.temp,
      'hum': instance.hum,
      'gasConcentration': instance.gasConcentration,
    };
