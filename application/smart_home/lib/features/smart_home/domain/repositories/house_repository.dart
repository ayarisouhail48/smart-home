import 'package:smart_home/core/api/models/api_response.dart';
import 'package:smart_home/features/smart_home/domain/entities/house.dart';

abstract class HouseRepository {
  Future<ApiResponse<List<House>?>> fetchHouses();
  Future<ApiResponse> registerHouse({
    required String name,
    required List<Map<String, dynamic>> devices,
  });
}
