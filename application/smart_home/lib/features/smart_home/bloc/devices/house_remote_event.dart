part of 'house_remote_bloc.dart';

abstract class HouseRemoteEvent extends Equatable {
  const HouseRemoteEvent();

  @override
  List<Object?> get props => [];
}

class FetchHouseData extends HouseRemoteEvent {
  final int houseId;

  const FetchHouseData({required this.houseId});

  @override
  List<Object> get props => [houseId];
}

class SwitchDevice extends HouseRemoteEvent {
  final Device device;
  final dynamic data;

  const SwitchDevice({required this.device, required this.data});

  @override
  List<Object> get props => [device, data];
}
