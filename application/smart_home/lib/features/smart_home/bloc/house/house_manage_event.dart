part of 'house_manage_bloc.dart';

abstract class HouseManageEvent extends Equatable {
  const HouseManageEvent();

  @override
  List<Object?> get props => [];
}

class FetchHouses extends HouseManageEvent {}

class RegisterHouse extends HouseManageEvent {
  final String name;
  final List<Map<String, dynamic>> devices;

  const RegisterHouse({
    required this.name,
    required this.devices,
  });

  @override
  List<Object> get props => [name, devices];
}
