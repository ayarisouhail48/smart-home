// import 'dart:async';
// import 'dart:math';
// import 'package:smart_home/features/authentication/domain/repositories/authentication/authentication_repository.dart';

// class AuthenticationRepositoryMock implements AuthenticationRepository {
//   final _authenticationController = StreamController<AuthenticationStatus>();

//   @override
//   Future<void> logIn({
//     required String username,
//     required String password,
//   }) async {
//     _authenticationController.add(AuthenticationStatus.loading);
//     int rand = Random().nextInt(10);
//     await Future.delayed(const Duration(milliseconds: 3000), () {
//       if (rand % 2 == 0) {
//         _authenticationController.add(AuthenticationStatus.unauthenticated);
//       } else {
//         _authenticationController.add(AuthenticationStatus.authenticated);
//       }
//     });
//   }

//   @override
//   void logOut() {
//     _authenticationController.add(AuthenticationStatus.unauthenticated);
//   }

//   @override
//   Stream<AuthenticationStatus> get status async* {
//     await Future<void>.delayed(const Duration(seconds: 1));
//     yield AuthenticationStatus.unauthenticated;
//     yield* _authenticationController.stream;
//   }

//   @override
//   void dispose() {
//     _authenticationController.close();
//   }
// }
