import 'dart:async';

import 'package:get_it/get_it.dart';
import 'package:smart_home/core/api/dio/dio_http_client.dart';
import 'package:smart_home/core/api/models/api_response.dart';
import 'package:smart_home/core/api/models/api_result.dart';
import 'package:smart_home/features/authentication/domain/entities/user.dart';
import 'package:smart_home/features/authentication/domain/repositories/user_repository.dart';

final GetIt sl = GetIt.instance;

class RestApiUserRepository implements UserRepository {
  final DioHttpClient _client = sl.get<DioHttpClient>();

  @override
  Future<ApiResponse<User?>> verifyUser({required String accessToken}) async {
    try {
      _client.configBearerToken(accessToken);

      var response = await _client.getMethod('/account/current', params: {});
      return ApiResponse.withResult(
        response: response.data,
        resultConverter: (json) => ApiResultSingle(
          json: json,
          rootName: '',
          jsonConverter: (json) => User.fromJson(json),
        ),
      );
    } catch (exception) {
      return ApiResponse.withError(exception);
    }
  }
}
