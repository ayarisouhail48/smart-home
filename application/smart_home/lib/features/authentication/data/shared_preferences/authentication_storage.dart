import 'package:smart_home/core/utils/shared_preferences_util/shared_preferences_utils.dart';

class AuthenticationStorage {
  static const String ACCESS_TOKEN = 'accessToken';
  static const String USERNAME = 'username';
  static const String PASSWORD = 'password';

  static String? get token =>
      SharedPreferencesUtils.getString(key: ACCESS_TOKEN);

  static String? get username =>
      SharedPreferencesUtils.getString(key: USERNAME);
  static String? get password =>
      SharedPreferencesUtils.getString(key: PASSWORD);

  static Future<void> updateToken(String accessToken) async =>
      await SharedPreferencesUtils.setString(
        key: ACCESS_TOKEN,
        value: accessToken,
      );

  static Future<void> updateCredentials({
    required String username,
    String? password,
  }) async {
    await SharedPreferencesUtils.setString(
      key: USERNAME,
      value: username,
    );
    if (password != null) {
      await SharedPreferencesUtils.setString(
        key: PASSWORD,
        value: password,
      );
    }
  }

  static Future<void> deleteCredentials() async {
    await SharedPreferencesUtils.removeString(
      key: PASSWORD,
    );
    await SharedPreferencesUtils.removeString(
      key: ACCESS_TOKEN,
    );
  }
}
