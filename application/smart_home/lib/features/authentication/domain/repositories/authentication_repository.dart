import 'dart:async';

import 'package:smart_home/core/api/models/api_response.dart';
import 'package:smart_home/features/authentication/domain/entities/auth.dart';
import 'package:smart_home/features/authentication/domain/entities/user.dart';

abstract class AuthenticationRepository {
  Future<ApiResponse<Auth>> logIn({
    required String username,
    required String password,
  });

  Future<ApiResponse<User>> signUp({
    required String email,
    required String username,
    required String password,
  });
}
