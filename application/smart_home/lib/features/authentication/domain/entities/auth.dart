import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'auth.g.dart';

@JsonSerializable(fieldRename: FieldRename.none)
class Auth extends Equatable {
  final String? accessToken;

  const Auth({
    this.accessToken,
  });

  @override
  List<Object> get props => [
        accessToken!,
      ];

  factory Auth.fromJson(Map<String, dynamic> json) => _$AuthFromJson(json);
  Map<String, dynamic> toJson() => _$AuthToJson(this);
}
