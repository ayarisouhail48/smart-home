import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:smart_home/core/utils/dialogs/loading_dialog.dart';
import 'package:smart_home/core/utils/snackbar.dart';
import 'package:smart_home/features/authentication/bloc/authentication_bloc.dart';

import '../../../../../router.dart';
import 'login_form.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Opacity(
            opacity: 0.2,
            child: Container(
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('lib/asset/login_background.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(24.0),
            child: Center(
              child: BlocListener<AuthenticationBloc, AuthenticationState>(
                listener: (context, state) {
                  switch (state.status) {
                    case AuthenticationStatus.loading:
                      showDialog(
                        barrierDismissible: false,
                        context: context,
                        builder: (context) => const LoadingDialog(
                          message: 'Logging in...',
                        ),
                      );
                      break;
                    case AuthenticationStatus.authenticated:
                      if (Navigator.canPop(context)) Navigator.pop(context);
                      Navigator.pushNamedAndRemoveUntil<void>(
                        context,
                        AppRouter.homePage,
                        (route) => false,
                      );
                      showSnackbarSH(
                        context,
                        'Login successful !',
                        AlertType.success,
                      );
                      break;
                    case AuthenticationStatus.failed:
                      if (Navigator.canPop(context)) Navigator.pop(context);
                      showSnackbarSH(
                        context,
                        'Login failed : "${state.errorMessage}" !',
                        AlertType.error,
                      );
                      break;
                    default:
                      break;
                  }
                },
                child: const LoginForm(),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
