import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:smart_home/core/utils/dialogs/loading_dialog.dart';
import 'package:smart_home/core/utils/snackbar.dart';
import 'package:smart_home/features/authentication/bloc/authentication_bloc.dart';
import 'package:smart_home/features/authentication/presentation/views/register/register_form.dart';

import '../../../../../router.dart';

class RegisterPage extends StatelessWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Opacity(
            opacity: 0.5,
            child: Container(
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('lib/asset/register_background.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(24.0),
            child: Center(
              child: BlocListener<AuthenticationBloc, AuthenticationState>(
                listener: (context, state) {
                  switch (state.status) {
                    case AuthenticationStatus.loading:
                      showDialog(
                        barrierDismissible: false,
                        context: context,
                        builder: (context) => const LoadingDialog(
                          message: 'Wait a second...',
                        ),
                      );
                      break;
                    case AuthenticationStatus.authenticated:
                      Navigator.pushNamedAndRemoveUntil<void>(
                        context,
                        AppRouter.login,
                        (route) => false,
                      );
                      showSnackbarSH(
                        context,
                        'Register Successful !',
                        AlertType.success,
                      );
                      break;
                    case AuthenticationStatus.failed:
                      if (Navigator.canPop(context)) Navigator.pop(context);
                      showSnackbarSH(
                        context,
                        state.errorMessage ?? 'ERROR: Register Failed !',
                        AlertType.error,
                      );
                      break;
                    default:
                      break;
                  }
                },
                child: const RegisterForm(),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
