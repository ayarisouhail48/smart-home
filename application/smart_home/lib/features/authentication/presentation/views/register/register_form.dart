import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:smart_home/core/common_widgets/password_form_field.dart';
import 'package:smart_home/core/common_widgets/text_form_field.dart';
import 'package:smart_home/core/common_widgets/widget_button.dart';
import 'package:smart_home/core/constants/color_const.dart';
import 'package:smart_home/core/utils/validator.dart';
import 'package:smart_home/features/authentication/bloc/authentication_bloc.dart';

import '../../../../../router.dart';

class RegisterForm extends StatefulWidget {
  const RegisterForm({Key? key}) : super(key: key);

  @override
  State<RegisterForm> createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  final _formKey = GlobalKey<FormState>();

  final spacerSmall = SizedBox(height: 0.02.sh);
  final spacerMedium = SizedBox(height: 0.04.sh);
  final spacerBig = SizedBox(height: 0.06.sh);

  String? firstName;
  String? lastName;
  String? email;
  String? username;
  String? password;
  String? passwordConfirm;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: SingleChildScrollView(
        child: Column(
          children: [
            _signupTitle(),
            spacerMedium,
            _firstnameInput(),
            spacerSmall,
            _lastnameInput(),
            spacerSmall,
            _emailInput(),
            spacerSmall,
            _usernameInput(),
            spacerSmall,
            _passwordInput(),
            spacerSmall,
            _passwordConfirmInput(),
            spacerMedium,
            _signupButton(),
            _backButton(),
          ],
        ),
      ),
    );
  }

  Widget _signupTitle() {
    return Align(
      alignment: Alignment.topLeft,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Sign Up',
            style: TextStyle(
              fontSize: 36.sp,
              fontWeight: FontWeight.bold,
              color: ColorConst.tealEnhanced,
            ),
          ),
          SizedBox(height: 8.h),
          Text(
            'Start smarten your home from now !',
            style: TextStyle(
              fontSize: 16.sp,
              fontWeight: FontWeight.w300,
              color: ColorConst.tealEnhanced,
            ),
          ),
        ],
      ),
    );
  }

  Widget _firstnameInput() {
    return TextFormFieldSH(
      labelText: 'First name',
      hintText: 'Eg: Nguyen, Le...',
      prefixIcon: Icon(
        Icons.person,
        size: 24.sp,
        color: ColorConst.tealEnhanced,
      ),
      onChanged: (value) => firstName = value,
      validator: (value) {
        if (!Validator.isValidNormalName(value!)) {
          return 'First name is invalid';
        }
      },
    );
  }

  Widget _lastnameInput() {
    return TextFormFieldSH(
      labelText: 'Last name',
      hintText: 'Eg: Hong Duc...',
      prefixIcon: Icon(
        Icons.person,
        size: 24.sp,
        color: ColorConst.tealEnhanced,
      ),
      onChanged: (value) => lastName = value,
      validator: (value) {
        if (!Validator.isValidNormalName(value!)) {
          return 'Last name is invalid';
        }
      },
    );
  }

  Widget _emailInput() {
    return TextFormFieldSH(
      labelText: 'Email',
      hintText: 'Eg: someone@example.org...',
      prefixIcon: Icon(
        Icons.email,
        size: 24.sp,
        color: ColorConst.tealEnhanced,
      ),
      onChanged: (value) => email = value,
      validator: (value) {
        if (!Validator.isValidEmail(value!)) {
          return 'Email is invalid';
        }
      },
    );
  }

  Widget _usernameInput() {
    return TextFormFieldSH(
      labelText: 'Username',
      hintText: 'Enter username...',
      prefixIcon: Icon(
        Icons.account_circle_sharp,
        size: 24.sp,
        color: ColorConst.tealEnhanced,
      ),
      onChanged: (value) => username = value,
      validator: (value) {
        if (!Validator.isValidUsername(value!)) {
          return 'Username is invalid';
        }
      },
    );
  }

  Widget _passwordInput() {
    return PasswordFormFieldSH(
      labelText: 'Password',
      hintText: 'Enter password...',
      prefixIcon: Icon(
        Icons.verified_user,
        size: 24.sp,
        color: ColorConst.tealEnhanced,
      ),
      onChanged: (value) => password = value,
      validator: (value) {
        if (!Validator.isValidPassword(value!)) {
          return 'Password is invalid';
        }
      },
    );
  }

  Widget _passwordConfirmInput() {
    return PasswordFormFieldSH(
      labelText: 'Re-enter Password',
      hintText: 'Confirm your password...',
      prefixIcon: Icon(
        Icons.verified_user,
        size: 24.sp,
        color: ColorConst.tealEnhanced,
      ),
      onChanged: (value) => passwordConfirm = value,
      validator: (value) {
        if (value! != password!) {
          return 'Password is not match';
        } else if (!Validator.isValidPassword(value)) {
          return 'Invalid password confirm !';
        }
      },
    );
  }

  Widget _signupButton() {
    return WidgetButtonSH(
      height: 40.h,
      onTap: () {
        FocusScope.of(context).unfocus();
        if (_formKey.currentState!.validate()) {
          context.read<AuthenticationBloc>().add(
                AuthenticationSignup(
                  email: email!,
                  username: username!,
                  password: password!,
                ),
              );
        }
      },
      child: Text(
        'Create Account',
        style: TextStyle(
          color: ColorConst.tealEnhanced,
          fontSize: 18.sp,
          fontWeight: FontWeight.w800,
        ),
      ),
    );
  }

  Widget _backButton() {
    return TextButton(
      onPressed: () {
        Navigator.pushReplacementNamed(context, AppRouter.login);
      },
      child: Text(
        'Back',
        style: TextStyle(
          color: ColorConst.tealEnhanced,
          fontSize: 18.sp,
          fontWeight: FontWeight.w800,
          decoration: TextDecoration.underline,
        ),
      ),
    );
  }
}
