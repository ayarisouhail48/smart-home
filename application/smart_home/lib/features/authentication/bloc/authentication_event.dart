part of 'authentication_bloc.dart';

abstract class AuthenticationEvent extends Equatable {
  const AuthenticationEvent();

  @override
  List<Object> get props => [];
}

class AuthenticationLogin extends AuthenticationEvent {
  final String username;
  final String password;

  const AuthenticationLogin({
    required this.username,
    required this.password,
  });

  @override
  List<Object> get props => [username, password];
}

class AuthenticationSignup extends AuthenticationEvent {
  // final String firstName;
  // final String lastName;
  final String email;
  final String username;
  final String password;

  const AuthenticationSignup({
    required this.email,
    required this.username,
    required this.password,
  });

  @override
  List<Object> get props => [email, username, password];
}

class AuthenticationAutoLogin extends AuthenticationEvent {}

class AuthenticationLogout extends AuthenticationEvent {}
