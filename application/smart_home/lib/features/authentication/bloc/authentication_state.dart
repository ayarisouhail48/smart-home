part of 'authentication_bloc.dart';

class AuthenticationState extends Equatable {
  const AuthenticationState._({
    this.status = AuthenticationStatus.unknown,
    this.user,
    this.errorMessage,
  });

  const AuthenticationState.unknown() : this._();

  const AuthenticationState.loading()
      : this._(status: AuthenticationStatus.loading);

  const AuthenticationState.failed(String errorMessage)
      : this._(status: AuthenticationStatus.failed, errorMessage: errorMessage);

  const AuthenticationState.authenticated(User user)
      : this._(status: AuthenticationStatus.authenticated, user: user);

  const AuthenticationState.unauthenticated()
      : this._(status: AuthenticationStatus.unauthenticated);

  final AuthenticationStatus status;
  final User? user;
  final String? errorMessage;

  @override
  List<Object> get props => [status];
}

enum AuthenticationStatus {
  unknown, // initial State
  loading, // triggered when action is on loading
  failed, // triggered when error occurs
  authenticated, // logged in or register successful
  unauthenticated, // logged out
}
