import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get_it/get_it.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smart_home/features/authentication/data/rest_api/user_repository_impl.dart';
import 'package:smart_home/features/smart_home/bloc/devices/house_remote_bloc.dart';
import 'package:smart_home/features/smart_home/bloc/house/house_manage_bloc.dart';
import 'package:smart_home/features/smart_home/presentation/views/home/home_page.dart';
import 'package:smart_home/router.dart';
import 'core/utils/dependency_injection/dependency_injection.dart';
import 'core/utils/shared_preferences_util/shared_preferences_utils.dart';
import 'features/authentication/bloc/authentication_bloc.dart';
import 'features/authentication/data/rest_api/authentication_repository_impl.dart';
import 'features/authentication/presentation/views/login/login_page.dart';
import 'features/authentication/presentation/views/register/register_page.dart';
import 'features/authentication/presentation/views/splash/splash_page.dart';
import 'features/smart_home/presentation/views/house/house_register_form.dart';

void runSmartHomeApp() async {
  WidgetsFlutterBinding.ensureInitialized();
  GetIt injector = GetIt.instance;
  await inject();
  await SharedPreferencesUtils.init();
  runApp(App(
    authenticationRepository: injector.get<RestApiAuthenticationRepository>(),
    userRepository: injector.get<RestApiUserRepository>(),
  ));
}

class App extends StatelessWidget {
  const App({
    Key? key,
    required this.authenticationRepository,
    required this.userRepository,
  }) : super(key: key);

  final RestApiAuthenticationRepository authenticationRepository;
  final RestApiUserRepository userRepository;

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<AuthenticationBloc>(
          create: (_) => AuthenticationBloc(
            authenticationRepository: authenticationRepository,
            userRepository: userRepository,
          ),
        ),
        BlocProvider<HouseManageBloc>(
          create: (_) => HouseManageBloc(),
        ),
        BlocProvider<HouseRemoteBloc>(
          create: (_) => HouseRemoteBloc(),
        ),
      ],
      child: const AppView(),
    );
  }
}

class AppView extends StatefulWidget {
  const AppView({Key? key}) : super(key: key);

  @override
  _AppViewState createState() => _AppViewState();
}

class _AppViewState extends State<AppView> {
  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(360, 690),
      minTextAdapt: true,
      splitScreenMode: true,
      builder: () => MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          fontFamily: GoogleFonts.mulish().fontFamily,
          primarySwatch: Colors.teal,
        ),
        builder: (context, widget) {
          ScreenUtil().setWidth(360);
          ScreenUtil().setHeight(690);
          ScreenUtil.setContext(context);
          return MediaQuery(
            data: MediaQuery.of(context).copyWith(
              textScaleFactor: 1.0,
            ),
            child: widget!,
          );
        },
        initialRoute: AppRouter.splash,
        routes: {
          AppRouter.splash: (_) => const SplashPage(),
          AppRouter.login: (_) => const LoginPage(),
          AppRouter.register: (_) => const RegisterPage(),
          AppRouter.homePage: (_) => const HomePage(),
          AppRouter.registerHouse: (_) => const HouseRegisterForm(),
        },
      ),
    );
  }
}
