import "package:dart_amqp/dart_amqp.dart";

void main() async {
  // // You can provide a settings object to override the
  // // default connection settings
  ConnectionSettings settings = ConnectionSettings(
    host: "localhost",
    port: 5672,
  );
  Client client = Client(settings: settings);

  Channel channel = await client.channel();
  Queue queue = await channel.queue(
    'queue.hust.iot.action',
    durable: true,
  );

  queue.publish('{"houseSensorId":92, "sensorType":"HUMIDITY","hum":92}');
  queue.publish('{"houseSensorId":93, "sensorType":"HUMIDITY","hum":88}');
  queue.publish(
      '{"houseSensorId":94, "sensorType":"GAS_CONCENTRATION","gasConcentration":11}');
  queue.publish(
      '{"houseSensorId":95, "sensorType":"GAS_CONCENTRATION","gasConcentration":6}');
  queue.publish('{"houseSensorId":82, "sensorType":"LED","status":true}');
  queue.publish('{"houseSensorId":83, "sensorType":"LED","status":true}');
  queue.publish('{"houseSensorId":84, "sensorType":"LED","status":false}');
  queue.publish('{"houseSensorId":87, "sensorType":"LED","status":false}');
  queue.publish('{"houseSensorId":88, "sensorType":"LED","status":true}');
  queue.publish('{"houseSensorId":89, "sensorType":"LED","status":false}');
  queue.publish('{"houseSensorId":90, "sensorType":"LED","status":false}');
  queue.publish('{"houseSensorId":91, "sensorType":"LED","status":false}');

  print(" [x] Sent");

  // Exchange exchange =
  //     await channel.exchange('queue.hust.iot.action', ExchangeType.TOPIC);
  // exchange.publish(
  //     '{"houseSensorId":3, "sensorType":"LED","status":"false"}', null);
  client.close();

  // _manager.publish('{"houseSensorId":3, "sensorType":"LED","status":"false"}');
}
